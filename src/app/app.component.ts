import { Component, HostListener } from '@angular/core';
import { AppService, Rectangle } from './app.service';
import { map } from 'rxjs/operators';
import { BehaviorSubject, combineLatest, Subject } from 'rxjs';

type SvgRect = Rectangle & { classes: string[] };

type SvgLine = { id: string; x1: number; y1: number; x2: number; y2: number };

const computeArrow = (
  source: Rectangle,
  target: Rectangle | MouseEvent
): SvgLine => {
  const x1 = source.x + source.width / 2;
  const y1 = source.y + source.height / 2;

  if (target instanceof MouseEvent) {
    const x2 = target.x;
    const y2 = target.y;

    return {
      id: `${source.id}->`,
      x1,
      y1,
      x2,
      y2,
    };
  }

  const x2 = target.x + target.width / 2;
  const y2 = target.y + target.height / 2;

  return {
    id: `${source.id}->${target.id}`,
    x1,
    y1,
    x2,
    y2,
  };
};

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  @HostListener('document:keydown.esc')
  onEscape(): void {
    this.appService.resetSelectedIds();
  }

  _hoveredIds = new BehaviorSubject<number[]>([]);

  _mousemove = new Subject<MouseEvent>();

  rectangles$ = combineLatest([
    this.appService.rectangles,
    this.appService.selectedIds,
    this._hoveredIds.asObservable(),
  ]).pipe(
    map(([rects, selectedIds, hoveredIds]) => {
      return rects.map((r) => {
        const result: SvgRect = { ...r, classes: [] };

        if (selectedIds.includes(r.id)) result.classes.push('selected');

        if (hoveredIds.includes(r.id)) result.classes.push('hovered');

        return result;
      });
    })
  );

  connections$ = combineLatest([
    this.appService.rectangles,
    this.appService.connections,
  ]).pipe(
    map(([rects, connections]) => {
      return connections.map((c) => {
        const source = rects.find(r => r.id === c.source) as Rectangle;
        const target = rects.find(r => r.id === c.target) as Rectangle;

        return computeArrow(source, target);
      });
    })
  );

  previewArrows$ = combineLatest([
    this.appService.rectangles,
    this.appService.selectedIds,
    this._hoveredIds.asObservable(),
    this._mousemove.asObservable(),
  ]).pipe(
    map(([rects, selectedIds, hoveredIds, mouseEvent]): SvgLine[] => {
      if (selectedIds.length === 0) return [];

      hoveredIds = hoveredIds.filter((item) => !selectedIds.includes(item));
      if (hoveredIds.length === 0)
        return selectedIds.map((selectedItem) => {
          const rect = rects.find((r) => r.id === selectedItem) as Rectangle;
          
          return computeArrow(rect, mouseEvent);
        });

      return selectedIds.reduce((acc, selectedItem) => {
        const rect = rects.find((r) => r.id === selectedItem) as Rectangle;

        return acc.concat(
          hoveredIds.map((hoveredItem) => {
            const hoveredRect = rects.find(
              (r) => r.id === hoveredItem
            ) as Rectangle;
            return computeArrow(rect, hoveredRect);
          })
        );
      }, [] as SvgLine[]);
    })
  );

  constructor(private readonly appService: AppService) {}

  trackById(_: number, item: { id: number }): number {
    return item.id;
  }

  onClick(event: MouseEvent, id?: number): void {
    event.stopPropagation();

    const selectedIds = this.appService.currentlySelectedIds;

    if (selectedIds.length > 0) {
      // Create connections for the targets
      if (id) {
        selectedIds.forEach(sId => this.appService.createConnection(sId, id));
        if (!event.ctrlKey) this.appService.resetSelectedIds();
      } else {
        this.appService.resetSelectedIds();
      }
    } else {
      if (id) this.appService.select([id]);
    }
  }

  onMouseover(id: number): void {
    const hovered = this._hoveredIds.getValue();

    this._hoveredIds.next(hovered.concat(id));
  }

  onMouseleave(id: number): void {
    const hovered = this._hoveredIds.getValue();

    this._hoveredIds.next(hovered.filter((h) => h !== id));
  }
}
