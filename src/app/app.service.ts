import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

export type Rectangle = {
  id: number;
  x: number;
  y: number;
  width: number;
  height: number;
};

@Injectable({
  providedIn: 'root'
})
export class AppService {
  private _rectangles = new BehaviorSubject<Array<Rectangle>>([
    { id: 1, x: 10, y: 10, width: 80, height: 20 },

    { id: 2, x: 120, y: 10, width: 50, height: 20 },
    { id: 3, x: 175, y: 10, width: 20, height: 20 },
    { id: 4, x: 120, y: 40, width: 70, height: 20 },

    { id: 5, x: 20, y: 100, width: 70, height: 20 },
    { id: 6, x: 110, y: 100, width: 80, height: 20 },
    { id: 7, x: 200, y: 100, width: 50, height: 20 },

    { id: 8, x: 20, y: 135, width: 25, height: 20 },
    { id: 9, x: 110, y: 135, width: 70, height: 20 },
    { id: 10, x: 200, y: 135, width: 75, height: 20 },

    { id: 11, x: 20, y: 165, width: 25, height: 20 },
    { id: 12, x: 110, y: 165, width: 75, height: 20 },
    { id: 13, x: 200, y: 165, width: 90, height: 20 },
  ]);

  private _selectedIds = new BehaviorSubject<number[]>([]);

  private _connections = new BehaviorSubject<Array<{source: number; target: number;}>>([]);

  get rectangles(): Observable<Array<Rectangle>> {
    return this._rectangles.asObservable();
  }

  get selectedIds(): Observable<Array<number>> {
    return this._selectedIds.asObservable();
  }

  get currentlySelectedIds(): Array<number> {
    return this._selectedIds.getValue();
  }
  
  get connections() {
    return this._connections.asObservable();
  }

  createConnection(sourceId: number, targetId: number): void {
    const currentConnections = this._connections.getValue();

    this._connections.next(currentConnections.concat({source: sourceId, target: targetId}))
  }

  /* Selection functions */
  select(ids: number[]): void {
    this._selectedIds.next(ids);
  }

  multiselect(ids: number[]): void {
    const selected = this._selectedIds.getValue();

    if (ids.every((id) => selected.includes(id)))
      // Ids are all in the selection already => remove them
      this._selectedIds.next(selected.filter((s) => !ids.includes(s)));
    // Some ids are not in the selection => add them and ignore already selected ids
    else
      this._selectedIds.next(
        selected.concat(ids.filter((id) => !selected.includes(id)))
      );
  }

  resetSelectedIds(): void {
    this._selectedIds.next([]);
  }
}
